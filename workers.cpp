#include <QMessageBox>

#include "workers.h"
#include "littlesolver.h"
#include "matrix.hpp"

using std::list;
using std::pair;

list<pair<size_t, size_t>> listToPairs(const list<size_t> &l) {
    list<pair<size_t, size_t>> p;

    if (l.empty())
        return p;

    auto iter1 = l.cbegin();
    auto iter2 = l.cbegin();
    ++iter2;
    while (iter2 != l.cend()) {
        p.emplace_back(*iter1, *iter2);
        ++iter1;
        ++iter2;
    }
    return std::move(p);
}

ThreadWorker::ThreadWorker(QObject *parent)
 :QObject(parent) {}

LittleWorker::LittleWorker(const Matrix<double> &m, QObject *parent)
    : ThreadWorker(parent), _solver(std::make_unique<LittleSolver>(m)) {

}

std::list<size_t> LittleWorker::getSolution() const {
    return _solver->getSolution();
}

double LittleWorker::getRecord() const {
    return _solver->getRecord();
}

LittleSolver::arclist LittleWorker::getLastStep() const {
    return _solver->getLastStep();
}

LittleSolver::arclist LittleWorker::getBestStep() const {
    return _solver->getBestStep();
}

void LittleWorker::process() {
    try {
    _solver->solve();
    }
    catch (std::exception &e) {
        QMessageBox::warning(NULL, "Error", e.what());
    }
    catch (const char *msg) {
        QMessageBox::warning(NULL, "Error", msg);
    }
    catch (...) {
        QMessageBox::warning(NULL, "Error", "Unknown error");
    }

    emit finished();
}

BruteWorker::BruteWorker(const Matrix<double> &m, QObject *parent)
: ThreadWorker(parent), _solver(m) {

}

std::list<size_t> BruteWorker::getSolution() const {
    return _solver.getSolution();
}

std::list<std::pair<size_t, size_t>> BruteWorker::getBestStep() const {
    return std::move(listToPairs(_solver.getSolution()));
}

std::list<std::pair<size_t, size_t>> BruteWorker::getLastStep() const {
    return std::move(listToPairs(_solver.getLastStep()));
}

double BruteWorker::getRecord() const {
    return _solver.getRecord();
}

void BruteWorker::process() {
    try {
    _solver.solve();
    }
    catch (std::exception &e) {
        QMessageBox::warning(NULL, "Error", e.what());
    }
    catch (const char *msg) {
        QMessageBox::warning(NULL, "Error", msg);
    }
    catch (...) {
        QMessageBox::warning(NULL, "Error", "Unknown error");
    }

    emit finished();
}
