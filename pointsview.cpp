#include <QMouseEvent>
#include <QKeyEvent>
#include <QPolygonF>
#include <QSize>
#include <math.h>

#include "pointsview.h"
#include "vertex.h"
#include "line.h"
#include <matrix.hpp>

using std::vector;
using std::list;
using std::pair;

PointsView::PointsView(QWidget *parent)
    :QGraphicsView(parent) {
    // �������� �����
    setScene(new QGraphicsScene());
    // ������ ����� ��� ������� �������
    adjustScene();

    // ��������� ������������
    setRenderHint(QPainter::Antialiasing);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setTransformationAnchor(AnchorUnderMouse);

    // ���������� �����������
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

PointsView::MatrixD PointsView::getDistanceMatrix() const {
    // �������� ������� ����������� ���������� ������
    MatrixD distances(vertexList.size());
    size_t i = 0, j = 0;
    for (const auto &iter1 : vertexList) {
        j = 0;
        for (const auto &iter2 : vertexList) {
            // ������� ����������
            distances(i, j) = sqrt(pow(iter1->x() - iter2->x(), 2) + pow(iter1->y() - iter2->y(), 2));
            ++j;
        }
        ++i;
    }

    return std::move(distances);
}

std::vector<std::pair<double, double>> PointsView::getPoints() const {
    // ����������
    vector<pair<double, double>> points;
    // ��������������� ��������� ������
    points.reserve(vertexList.size());

    for (const auto &i : vertexList) {
        points.emplace_back(i->x(), i->y());
    }

    return std::move(points);
}

void PointsView::clearVerteces() {
    while (!vertexList.empty()) {
        // ����� �����������
        delete vertexList.front();
        // �������� ���������
        vertexList.pop_front();
    }
}

void PointsView::clearEdges() {
    while (!edgesList.empty()) {
        // ����� �����������
        delete edgesList.front();
        // �������� ���������
        edgesList.pop_front();
    }
}

void PointsView::showPath(const list<size_t> &path,  QColor color) {
    if (path.empty())
        return;

    // ��������� �� 2 �������� ��������
    auto iter1 = path.cbegin();
    auto iter2 = iter1;
    ++iter2;

    while (iter2 != path.cend()) {
        // ���������� ����� ����� ���������
        addLine(vertexList.at(*iter1)->pos(),
                vertexList.at(*iter2)->pos(),
                color);
        // ������� �� ��������� ����
        ++iter1;
        ++iter2;
    }
}

void PointsView::showPath(const std::list<std::pair<size_t, size_t> > &path, QColor color) {
    if (path.empty())
        return;

    for (const auto &iter : path)
        // ���������� ����� ����� ��������� �� �������� �������
        addLine(vertexList.at(iter.first)->pos(),
                vertexList.at(iter.second)->pos(),
                color);
}

void PointsView::mouseMoveEvent(QMouseEvent *event) {
    QGraphicsView::mouseMoveEvent(event);
    if (event->buttons()) {
        // ������� ��� �����, ���� ���� ���������� ���� � ������� ��������
        clearEdges();
    }
}

void PointsView::mousePressEvent(QMouseEvent *event) {
    QGraphicsView::mousePressEvent(event);
}

void PointsView::mouseDoubleClickEvent(QMouseEvent *event) {
    addVertex(mapToScene(event->pos()));
}

void PointsView::keyPressEvent(QKeyEvent *event) {
    // ���� ������ ������� Delete
    if (event->key() == Qt::Key_Delete) {
        // �������� ���� ���������� ������
        for (QList<Vertex *>::iterator iter = vertexList.begin(); iter != vertexList.end(); ++iter)
            if ((*iter)->isSelected())
            {
                delete *iter;
                iter = vertexList.erase(iter);
                --iter;
            }
        // ������� ������ �����
        clearEdges();
    }
}

void PointsView::mouseReleaseEvent(QMouseEvent *event) {
    QGraphicsView::mouseReleaseEvent(event);
    // ������� ������, ������� ����� �� ������� �����
    correctVertexPositions();
}

void PointsView::resizeEvent(QResizeEvent *event) {
    QGraphicsView::resizeEvent(event);
    // ������ �������� �����
    adjustScene();
    // ������� ������, ������� ����� �� �������
    correctVertexPositions();
    // ������ �� ��������� �������
    emit resized();
}

void PointsView::correctVertexPositions()
{
    for (QList<Vertex *>::iterator iter = vertexList.begin(); iter != vertexList.end(); ++iter)
    {
        // ���� ������� �����
        if ((*iter)->pos().x() - Vertex::RADIUS < 0)
            (*iter)->setX(Vertex::RADIUS);
        // ���� ������� ������
        if ((*iter)->pos().y() - Vertex::RADIUS < 0)
            (*iter)->setY(Vertex::RADIUS);
        // ���� ������� ������
        if ((*iter)->pos().x() + Vertex::RADIUS > sceneRect().width())
            (*iter)->setX(sceneRect().width() - Vertex::RADIUS);
        // ���� ������� �����
        if ((*iter)->pos().y() + Vertex::RADIUS > sceneRect().height())
            (*iter)->setY(sceneRect().height() - Vertex::RADIUS);
    }
}

void PointsView::adjustScene() {
    // ��������� �������� �����
    scene()->setSceneRect(this->rect());
}

void PointsView::addVertex(QPointF pos) {
    // �������� ������� � �������� �������
    Vertex *v = new Vertex(vertexList.size());

    // ������� ���������
    v->setPos(pos);
    // ���������� �� �����
    scene()->addItem(v);
    // ���������� � ������ ����������
    vertexList.push_back(v);

    // �������� ���� �����
    clearEdges();
}

void PointsView::addLine(QPointF p1, QPointF p2, QColor color) {
    // �������� ����� ��������� ����� ����� ������������
    Line *line = new Line(p1, p2, color);
    // ���������� �� �����
    scene()->addItem(line);
    // ���������� � ������ ����������
    edgesList.push_back(line);
}

