#pragma once

#include <QObject>
#include <memory>
#include "bruteforcesolver.h"
#include "littlesolver.h"

template<typename T>
class Matrix;

class ThreadWorker: public QObject {
    Q_OBJECT
public:
    explicit ThreadWorker(QObject *parent = 0);

    virtual std::list<size_t> getSolution() const = 0;
    virtual std::list<std::pair<size_t, size_t>> getLastStep() const = 0;
    virtual std::list<std::pair<size_t, size_t>> getBestStep() const = 0;

    virtual double getRecord() const = 0;

public slots:
    virtual void process() = 0;

signals:
    virtual void finished();
};

class LittleWorker : public ThreadWorker
{
    Q_OBJECT
public:
    explicit LittleWorker(const Matrix<double> &m, QObject *parent = 0);

    std::list<size_t> getSolution() const override;
    LittleSolver::arclist getLastStep() const override;
    LittleSolver::arclist getBestStep() const override;
    double getRecord() const override;

public slots:
    void process() override;

signals:
    void finished();

private:
    std::unique_ptr<LittleSolver> _solver;
};

class BruteWorker : public ThreadWorker
{
    Q_OBJECT
public:
    explicit BruteWorker(const Matrix<double> &m, QObject *parent = 0);

    std::list<size_t> getSolution() const override;
    std::list<std::pair<size_t, size_t>> getLastStep() const override;
    std::list<std::pair<size_t, size_t>> getBestStep() const override;
    double getRecord() const override;

public slots:
    void process() override;

signals:
    void finished() override;

private:
    BruteforceSolver _solver;
};
