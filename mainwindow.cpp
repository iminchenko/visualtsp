#include <QMessageBox>
#include <QThread>
#include <random>
#include <ctime>
#include <time.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "workers.h"
#include "matrix.hpp"
#include "bruteforcesolver.h"

const QImage sceneImage(QGraphicsScene *const scene) {
    QImage image(scene->itemsBoundingRect().size().toSize(),
                 QImage::Format_ARGB32);
    image.fill(Qt::white);

    QPainter painter(&image);
    painter.setRenderHint(QPainter::Antialiasing);
    scene->render(&painter, image.rect(), scene->itemsBoundingRect());
    return image;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    _processing = false;
    srand(time(NULL));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_solveBrute_clicked() {
    if (_processing)
        return;
    _iteration = 0;
    _processing = true;
    ui->graphicsView->clearEdges();
    ui->graphicsView->setEnabled(false);

    _method = METHOD_BRUTE;
    QThread *thread = new QThread;
    _worker = new BruteWorker(ui->graphicsView->getDistanceMatrix());
    _worker->moveToThread(thread);
    connect(thread, SIGNAL(started()), _worker, SLOT(process()));
    connect(_worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), this, SLOT(solutionFinished()));
    _start = std::clock();
    thread->start();
    _solvingTimerId = startTimer(70);
}

void MainWindow::on_pushButton_solveLittle_clicked() {
    if (_processing)
        return;
    _iteration = 0;
    _processing = true;
    ui->graphicsView->setEnabled(false);
    ui->graphicsView->clearEdges();
    _method = METHOD_LITTLE;
    QThread *thread = new QThread;
    _worker = new LittleWorker(ui->graphicsView->getDistanceMatrix());
    _worker->moveToThread(thread);
    connect(thread, SIGNAL(started()), _worker, SLOT(process()));
    connect(_worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), this, SLOT(solutionFinished()));
    _start = std::clock();
    thread->start();
    _solvingTimerId = startTimer(70);
}

void MainWindow::on_pushButton_clear_clicked() {
    if (_processing)
        return;

    ui->graphicsView->clearVerteces();
    ui->lineEdit_result->clear();
}

void MainWindow::timerEvent(QTimerEvent *event) {
    if (event->timerId() == _solvingTimerId) {
//        sceneImage(ui->graphicsView->scene()).save("pics/iteration" + QString::number(_iteration) + ".png");
        ++_iteration;
        ui->lineEdit_time->setText(QString::number(double(std::clock() - _start)/CLOCKS_PER_SEC));
        if (ui->checkBox->isChecked()) {
            ui->graphicsView->clearEdges();
            ui->graphicsView->showPath(_worker->getBestStep(), Qt::yellow);
            ui->graphicsView->showPath(_worker->getLastStep(), Qt::black);
            ui->lineEdit_result->setText(QString::number(_worker->getRecord()));
        }
    }
}

void MainWindow::solutionFinished() {
    ui->lineEdit_time->setText(QString::number(double(std::clock() - _start)/CLOCKS_PER_SEC));
    _processing = false;
    ui->graphicsView->setEnabled(true);
    killTimer(_solvingTimerId);
    ui->graphicsView->clearEdges();

    ui->graphicsView->showPath(_worker->getSolution());
    ui->lineEdit_result->setText(QString::number(_worker->getRecord()));
//    for (int i = 0; i < 10; ++i) {
//        sceneImage(ui->graphicsView->scene()).save("pics/iteration" + QString::number(_iteration) + ".png");
//        ++_iteration;
//    }

    delete _worker;
}

void MainWindow::on_horizontalSlider_valueChanged(int value) {
    if (_processing)
        return;
    // generating poinst
    ui->graphicsView->clearEdges();
    ui->graphicsView->clearVerteces();
    ui->label_3->setText(QString::number(value));

    while (value--) {
        ui->graphicsView->addVertex(QPointF(rand() % (ui->graphicsView->width() - 20) + 10,
                                        rand() % (ui->graphicsView->height() - 20) + 10));
    }
}
