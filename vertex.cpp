#include <QPainter>

#include "vertex.h"

Vertex::Vertex(int number)
{
    _number = number;
    setZValue(1);
    setFlag(ItemIsMovable);
    setFlag(ItemIsSelectable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setAcceptHoverEvents(true);
}

void Vertex::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (isSelected())
        painter->setBrush(QBrush(Qt::red));
    else
        painter->setBrush(QBrush(Qt::black));
    painter->drawEllipse(boundingRect());
}

void Vertex::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverEnterEvent(event);
    setToolTip(QString::number(_number) + " (" + QString::number(pos().x()) + ", " + QString::number(pos().y()) + ")");
}

QRectF Vertex::boundingRect() const
{
    return QRectF(-RADIUS, -RADIUS, 2*RADIUS, 2*RADIUS);
}
