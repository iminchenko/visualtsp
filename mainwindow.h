#pragma once

#include <QMainWindow>
#include <ctime>

class ThreadWorker;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_solveBrute_clicked();

    void on_pushButton_solveLittle_clicked();

    void on_pushButton_clear_clicked();

    void timerEvent(QTimerEvent *event);

    void solutionFinished();

    void on_horizontalSlider_valueChanged(int value);

private:
    Ui::MainWindow *ui;

    enum {METHOD_BRUTE, METHOD_LITTLE} _method;
    bool _processing;
    ThreadWorker *_worker;
    int _solvingTimerId;
    clock_t _start;
    int _iteration;
};
