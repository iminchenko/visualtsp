#pragma once

#include <QGraphicsView>
#include <QList>
#include <vector>
#include <list>

class Vertex;
class Line;

template<typename T>
class Matrix;

class PointsView: public QGraphicsView
{
    Q_OBJECT
public:
    using MatrixD = Matrix<double>;

    PointsView(QWidget *parent = NULL);

    // �������� ������� ����������
    MatrixD getDistanceMatrix() const;
    // �������� ������ ��������� �����
    std::vector<std::pair<double, double> > getPoints() const;

    // ������� ��� �������
    void clearVerteces();
    // ������� ��� �����
    void clearEdges();
    // �������� ������� � ��������� ������������
    void addVertex(QPointF pos);
 
    // ����� ���� ����� ��������� � ������� ��������� �����
    void showPath(const std::list<size_t> &path, QColor color = Qt::black);
    void showPath(const std::list<std::pair<size_t, size_t> > &path, QColor color = Qt::black);

protected:
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    // need to remove drawed polydon
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    // need to add polygon
    void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    // need to move vertexes outside scene rect
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    // need to draw polydon or remove drawed polydon
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    // need to move vertexes outside scene rect and adjust scene
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

    // need to move vertexes outside scene rect
    void correctVertexPositions();
    // ������ �������� �����
    void adjustScene();
    // ���������� ����� ����� ���������
    void addLine(QPointF p1, QPointF p2, QColor color = Qt::black);

private:
    // ������ ���� ������ �����
    QList<Vertex *> vertexList;
    // ������ �������� �����
    QList<Line *> edgesList; 

signals:
    void resized();
};
