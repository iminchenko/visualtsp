#include <QPainter>

#include "line.h"
#include "vertex.h"

Line::Line(QPointF p1, QPointF p2, QColor color) {
    _p1 = p1;
    _p2 = p2;
    _color = color;
}

void Line::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                 QWidget *widget) {
    this->update();
    QPen pen(_color);
    pen.setWidth(2);
    painter->setPen(pen);
    painter->drawLine(_p1, _p2);
}

QRectF Line::boundingRect() const {
    return QRectF(_p1, QSizeF(_p2.x() - _p1.x(),
                              _p2.y() - _p1.y())).normalized();
}
