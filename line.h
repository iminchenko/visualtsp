#pragma once

#include <QGraphicsItem>

class Vertex;

class Line : public QGraphicsItem {
public:
    Line(QPointF p1, QPointF p2, QColor color = Qt::black);

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

    QRectF boundingRect() const Q_DECL_OVERRIDE;

private:
    QPointF _p1, _p2;
    QColor _color;
};
