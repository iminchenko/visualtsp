#pragma once

#include <QGraphicsItem>


class Vertex : public QGraphicsItem
{
public:
    enum {RADIUS = 5};

    Vertex(int number);

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    // sets to tooltip current position on mouse hover event
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;

    QRectF boundingRect() const Q_DECL_OVERRIDE;

    int _number;
};
